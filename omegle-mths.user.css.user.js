// ==UserScript==
// @name        User style: Omegle mths
// @description Because the Stylish team are asshole, and only permit installing from userstyles.org by default. And DOES NOT allow auto update.
// @include     *omegle.com*
// @version     1.1
// @downloadURL https://bitbucket.org/mths/userstyles/raw/master/omegle-mths.user.css.user.js
// @updateURL   https://bitbucket.org/mths/userstyles/raw/master/omegle-mths.user.css.user.js
// ==/UserScript==

function loadcss(css) {
	// http://stackoverflow.com/questions/524696/how-to-create-a-style-tag-with-javascript
	var head = document.head || document.getElementsByTagName('head')[0],
		style = document.createElement('style');

	style.type = 'text/css';
	if (style.styleSheet){
		style.styleSheet.cssText = css;
	} else {
		style.appendChild(document.createTextNode(css));
	}
	head.appendChild(style);
}

loadcss(`
    /* Edit here, to fix background and have another main backgorund color */
    body {
        background-image: url(https://i.imgur.com/d9vxham.jpg); /* © Sergey Dolya - http://www.pickywallpapers.com/around-the-world/burma/smith-s-authentic-hammer-picture/ */
    }
    .logwrapper,.disconnectbtn,.chatmsgwrapper,.sendbtn,#header,#intro {
        background-color: rgba(10,10,10,0.7); 
    }
    
    
    /* Video chat looks ugly AF in this skin, sorry, better disable the things to avoid confusion. Might make it available in an update if enough request.. */
    .chattypeorcell, #videobtn, #monitoringnotice, #videobtnstatus {
        display: none;
    }
    
    
    /* The other stuff */
    body {
        background-attachment: fixed;
        background-repeat: no-repeat;
        background-color: black;
        background-size: cover;
        background-position: center; 
        margin: auto;
        width: 960px;
    }
    .logwrapper {
        left:auto;
        right:auto;
        margin: auto;
        width: 960px;
    }
    .logwrapper {
        border: 0px solid #330000 !important;
    } 
    .controlwrapper {
        left:auto;
        right:auto;
        margin: auto;
        width: 960px;
        opacity: 0.98;
    }
    .strangermsg .msgsource{
        color: #8B3A3A !important;
    }
    .youmsg .msgsource{
        color: #566E78 !important;
    }
    .logbox {
        color: #9E9E9E !important;
    }
    .disconnectbtn,.sendbtn {
        color: #666 !important;
        border-radius: 0px;
    }
    .disconnectbtnwrapper,.sendbtnwrapper {
        background-color: transparent;
        border-radius: 0px;
        border:0px solid black !important;
    } 
    .chatmsg {
        color: #9E9E9E !important;
        background-color: transparent;
    }
    .chatmsgwrapper {
        border: 0px solid #333 !important;
    } 
    a:link {
        color: #566E78 !important;
    }
    #header,.chatmsgwrapper,.disconnectbtnwrapper,.sendbtnwrapper,.logwrapper {
        -webkit-box-shadow: 0px 0px 17px 0px rgba(0, 0, 0, 0.95);
        -moz-box-shadow:    0px 0px 17px 0px rgba(0, 0, 0, 0.95);
        box-shadow:         0px 0px 17px 0px rgba(0, 0, 0, 0.95);
    }
    #tagline > img,#fb-root,.fb-like,.twitter-share-button, #feedback,#topterms,#adwrapper,#mobilesitenote,#introtext {
        display: none;
    }
    #onlinecount {
        color: #5b7a99;
    }
    #onlinecount > strong {
        color: #7ba4ce;
    }
    #intro {
        position: absolute;
        left: auto;
        right: auto;
        border: 0px;
        width: 950px;
    }
    label, #startachat, #intoheader, #google_translate_lower_container {
        color: white;
    }
    .conversationgreat{
        background-color: #786056 !important;
        color: #222 !important;
    }
    .statuslog input {
        opacity: 0.5 !important;
    }
    .topictageditor {
        color: black;
        background: rgba(128,120,120,0.7) !important;   
    }
    .newchatbtnwrapper > img, #videobtn, #textbtn { 
        filter: invert(100%);
    }
    .logitem > .question > .questionHeading {
        background: rgba(255,182,108,0.3);
    }
    .logitem > .question {
        width:100%
    }
    .logitem > .question > .questionText {
        background: rgba(255,238,221,0.15);
    }
    #tryspymode > div > form > div > label {
        color: black;
    }
    .goog-te-gadget {
        opacity: 0.2;
    }
`);

setTimeout(function() {document.getElementsByClassName("fb-like")[0].style.display = 'none';}, 1234); // Alright alright, Stylish would have been more easy. But I still miss those install from other url and auto update feature..