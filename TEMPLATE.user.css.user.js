// ==UserScript==
// @name        User style: TEMPLATE
// @description Because the Stylish team are asshole, and only permit installing from userstyles.org by default. And DOES NOT allow auto update.
// @include     *domain.tld*
// @version     1.0
// @downloadURL https://bitbucket.org/mths/userstyles/raw/master/TEMPLATE.user.css.user.js
// @updateURL   https://bitbucket.org/mths/userstyles/raw/master/TEMPLATE.user.css.user.js
// ==/UserScript==

function loadcss(css) {
	// http://stackoverflow.com/questions/524696/how-to-create-a-style-tag-with-javascript
	var head = document.head || document.getElementsByTagName('head')[0],
		style = document.createElement('style');

	style.type = 'text/css';
	if (style.styleSheet){
		style.styleSheet.cssText = css;
	} else {
		style.appendChild(document.createTextNode(css));
	}
	head.appendChild(style);
}

loadcss(`
body { 
	background: yellow; 
}
`);